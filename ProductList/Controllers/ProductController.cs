﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProductList.Models;
using ProductList.Models.Pages;

namespace ProductList.Controllers
{
    [Authorize]
    public class ProductController : Controller
    {
        IRepository repository;

        public ProductController(IRepository repo)
        {
            repository = repo;
        }

        public IActionResult List(QueryOptions options)
        {
            return View(repository.GetProducts(options));
        }

        public IActionResult Edit(int key)
        {
            return View(repository.GetProduct(key));
        }

        [HttpPost]
        public IActionResult Edit(Product product)
        {
            if (ModelState.IsValid)
            {
                repository.UpdateProduct(product);
                TempData["message"] = $"{product.Name} has been saved";
                return RedirectToAction(nameof(List));
            }
            else
            {
                return View(product);
            }
        }
        public IActionResult Create() => View("Edit", new Product());
    }
}
