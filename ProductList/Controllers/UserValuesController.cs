﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ProductList.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ProductList.Controllers
{
    [Route("api/users")]
    public class UserValuesController : Controller
    {
        private IUserRepository repository;
        private TimeService timeService;

        public UserValuesController(IUserRepository repo, TimeService tS)
        {
            repository = repo;
            timeService = tS;
        }
        [HttpPost]
        public IActionResult CreateUser([FromBody]User user)
        {

            if (ModelState.IsValid && user.BirthDate <= timeService.GetTime())
            {
                return Ok(repository.CreateUser(user));
            }
            return BadRequest(ModelState);
        }
    }
}
