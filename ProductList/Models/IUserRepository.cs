﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductList.Models
{
    public interface IUserRepository
    {
        int CreateUser(User user);
    }
}
