﻿using ProductList.Models.Pages;

namespace ProductList.Models
{
    public interface IRepository
    {
        Product GetProduct(int key);

        PagedList<Product> GetProducts(QueryOptions options);

        void UpdateProduct(Product product);
    }
}
