﻿namespace ProductList.Models
{
    public interface IWebServiceRepository
    {
        object GetProduct(int id);

        object GetProducts(FilterModel filter);
    }
}
