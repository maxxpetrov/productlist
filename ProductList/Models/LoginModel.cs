﻿using System.ComponentModel.DataAnnotations;

namespace ProductList.Models
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Please enter a Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please enter a password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
