﻿namespace ProductList.Models
{
    public class FilterModel
    {
        public string Term { get; set; }
        public int? minPrice { get; set; }
        public int? maxPrice { get; set; }
    }
}
