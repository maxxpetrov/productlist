﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductList.Models
{
    public class UserRepository : IUserRepository
    {
        private DataContext context;

        public UserRepository(DataContext ctx)
        {
            context = ctx;
        }

        public int CreateUser(User user)
        {
            context.Users.Add(user);
            context.SaveChanges();
            return user.Id;
        }
    }
}
