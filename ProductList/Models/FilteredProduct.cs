﻿namespace ProductList.Models
{
    public class FilteredProduct
    {
        public int ID { get; set; }        
        public string Name { get; set; }
        public decimal Price { get; set; }
    }
}
