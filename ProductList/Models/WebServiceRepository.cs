﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ProductList.Models
{
    public class WebServiceRepository : IWebServiceRepository
    {
        private DataContext context;
        public WebServiceRepository(DataContext ctx) => context = ctx;

        public object IEnumarable { get; private set; }

        public object GetProduct(int id)
        {
            return context.Products.FirstOrDefault(p => p.ProductID == id);
        }

        public object GetProducts(FilterModel filter)
        {

            return context.Products.Where(p => p.Name.StartsWith(filter.Term ?? string.Empty, StringComparison.InvariantCultureIgnoreCase)
                                                    && p.Price >= (filter.minPrice ?? 0)
                                                    && p.Price <= (filter.maxPrice ?? int.MaxValue))
                                    .Select(p => new FilteredProduct { ID = p.ProductID, Name = p.Name, Price = p.Price});
        }
    }
}
